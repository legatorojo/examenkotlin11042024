package com.example.myapplication

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.myapplication.adapter.GenerosAdapter
import com.example.myapplication.databinding.DetalleActivityBinding
import com.example.myapplication.viewmodel.DataViewModel
import androidx.lifecycle.Observer

class DetalleActivity: AppCompatActivity() {

    private lateinit var binding: DetalleActivityBinding
    val viewModel: DataViewModel by viewModels()
    var idPelicula = 0L

    private val adapterData by lazy {
        GenerosAdapter(this)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DetalleActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)


        idPelicula = intent.getLongExtra("idPelicula", 0)
        Log.d("Examen", "ID Pelicula:" + idPelicula);


        viewModel.getMovieResponse.observe(this, Observer { data->


            if(data!= null && data.title != null)
            {
                Log.d("Examen", "Pelicula encontrada:" + data.title);

                Glide.with(this)
                    .load(GeneralVariables.baseUrlImage + data.backdrop_path)
                    .into(binding.imagen);

                binding.titulo.text = data.title
                binding.calificacion.text = data.vote_average.toString()
                binding.descripcion.text = data.overview
                binding.duracionLabel.text = "" + data.runtime + " min"
                binding.fechaEstreno.text = data.release_date

                adapterData.apply {
                    dataSet = data.genres
                }

                val llm = LinearLayoutManager(this)
                llm.orientation = LinearLayoutManager.VERTICAL
                binding.generos.setLayoutManager(llm)
                binding.generos.adapter = adapterData
            }


        })

        viewModel.getMovieData(idPelicula)

        binding.botonAtras.setOnClickListener {

            finish()
        }

    }



}