package com.example.myapplication

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.adapter.MoviesAdapter
import com.example.myapplication.databinding.ListadoActivityBinding
import com.example.myapplication.viewmodel.DataViewModel
import androidx.lifecycle.Observer
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth

class ListadoActivity: AppCompatActivity() {

    private lateinit var binding: ListadoActivityBinding
    val viewModel: DataViewModel by viewModels()
    private lateinit var auth: FirebaseAuth

    private val adapterData by lazy {
        MoviesAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ListadoActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if(GeneralVariables.logueado == false)
        {
            GeneralVariables.logueado = true
            Toast.makeText(baseContext, "Bienvenido al listado de películas", Toast.LENGTH_LONG).show()
        }

        auth = Firebase.auth


        viewModel.getMoviesResponse.observe(this, Observer { data->

            Log.d("Examen", "Datos:" + data.results.size);


            adapterData.apply {
                dataSet = data.results
            }


            val llm = LinearLayoutManager(this)
            llm.orientation = LinearLayoutManager.VERTICAL
            binding.peliculas.setLayoutManager(llm)
            binding.peliculas.adapter = adapterData



        })


        viewModel.getMovies()


        binding.salir.setOnClickListener {

            auth.signOut()
            GeneralVariables.logueado = false
            finish()

        }

    }


}