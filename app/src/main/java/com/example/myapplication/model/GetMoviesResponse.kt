package com.example.myapplication.model

data class GetMoviesResponse (

    var page: Int? = 0,
    val results: List<MovieData> = listOf(),
    var total_pages: Int? = 0,
    var total_results: Int? = 0

)