package com.example.myapplication.model

data class MovieData (

    var title: String? = "",
    var vote_average: Double? =  0.0,
    var backdrop_path: String? = "",
    var id: Long? = 0

)