package com.example.myapplication.model

data class GetMovieResponse (

    var backdrop_path: String? = "",
    var title: String? = "",
    var runtime: Int? = 0,
    var release_date: String? = "",
    var vote_average : Double? = 0.0,
    var overview: String? = "",
    val genres: List<Genero> = listOf(),

)