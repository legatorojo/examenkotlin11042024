package com.example.myapplication.model

data class Genero(

    var id: Int? = 0,
    var name: String? = ""

)