package com.example.myapplication.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.model.GetMovieResponse
import com.example.myapplication.model.GetMoviesResponse
import com.example.myapplication.repository.MoviesRepository
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class DataViewModel: ViewModel() {

    private val repository = MoviesRepository()

    val getMoviesResponse = MutableLiveData<GetMoviesResponse>()
    val getMovieResponse = MutableLiveData<GetMovieResponse>()


    fun getMovies(
    ) = viewModelScope.launch {
        repository.GetListMovies(
        ).collectLatest { repositoryResponse ->

            getMoviesResponse.postValue(repositoryResponse)

        }
    }


    fun getMovieData(
        id: Long
    ) = viewModelScope.launch {
        repository.GetMovieData(
            id = id
        ).collectLatest { repositoryResponse ->

            getMovieResponse.postValue(repositoryResponse)

        }
    }


}