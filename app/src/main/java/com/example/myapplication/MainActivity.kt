package com.example.myapplication


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.myapplication.databinding.ActivityMainBinding
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = Firebase.auth


        /*
        *
        * Usuario valido
        *
        * Correo: jivanflorescruz@gmail.com
        * Contraseña: 123456
        *
        * */
        binding.botonInicia.setOnClickListener(){

            if(binding.correo.text.toString().equals("") || binding.contrasena.text.toString().equals(""))
            {
                Toast.makeText(baseContext, "Debe ingresar usuario y/o contraseña", Toast.LENGTH_LONG).show()
            }
            else
            {

                auth.signInWithEmailAndPassword(binding.correo.text.toString(), binding.contrasena.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful)
                        {
                            startActivity(Intent(this, ListadoActivity::class.java).apply {
                            })
                        }
                        else
                        {
                            ErrorMsg()
                        }

                    }
                    .addOnFailureListener {

                        ErrorMsg()

                    }

            }

        }


    }


    fun ErrorMsg()
    {
        Toast.makeText(baseContext, "Error al ingresar, verifique usuario y/o contraseña", Toast.LENGTH_LONG).show()
    }


}