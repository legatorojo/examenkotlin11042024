import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

class RetrofitInstance {
    companion object {
        private val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.HEADERS
        }

        private val client: OkHttpClient = OkHttpClient.Builder().apply {
            readTimeout(20, TimeUnit.SECONDS)
            connectTimeout(20, TimeUnit.SECONDS)
            addInterceptor(interceptor)
            addInterceptor(RetryInterceptor(2))

        }.build()

        fun getRetrofitInstance(): Retrofit = Retrofit.Builder().baseUrl("https://api.themoviedb.org")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).client(client).build()
    }

    private class RetryInterceptor(private val maxRetries: Int) : Interceptor {
        var response: Response? = null
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()
                .newBuilder()
                .build()

            var retryCount = 0
            var requestSuccess = false

            while (!requestSuccess && retryCount <= maxRetries) {
                try {
                    response?.close()
                    response = chain.proceed(request)
                    requestSuccess = response?.isSuccessful ?: false
                    retryCount++
                } catch (e: IOException) {
                    response?.close()
                    retryCount++
                    if (retryCount > maxRetries) {
                        throw e
                    }
                }
            }
            return response ?: throw IOException("Null response received")
        }
    }
}
