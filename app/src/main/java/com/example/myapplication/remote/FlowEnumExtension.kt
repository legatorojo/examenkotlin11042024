package com.example.myapplication.remote


private const val baseUrl = "https://api.themoviedb.org/3/movie/"
private const val apiKey = "api_key=c0823934438075d63f1dbda4023e76fc"

fun FlowEnum.url(id: Array<String>? = emptyArray()) = when (this){


    FlowEnum.GET_LISTADO        -> "$baseUrl/now_playing?$apiKey"
    FlowEnum.GET_DATA           -> "$baseUrl/${id?.get(0)}?$apiKey"

}