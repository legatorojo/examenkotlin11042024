package com.example.examenkotlin27032024.Remote

import com.example.myapplication.model.GetMovieResponse
import com.example.myapplication.model.GetMoviesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface GetDataApi {


    @GET
    suspend fun getListMovies(
            @Url url: String
    ): Response<GetMoviesResponse>


    @GET
    suspend fun getMovieData(
        @Url url: String
    ): Response<GetMovieResponse>


}