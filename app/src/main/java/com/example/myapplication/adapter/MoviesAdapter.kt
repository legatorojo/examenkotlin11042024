package com.example.myapplication.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.DetalleActivity
import com.example.myapplication.GeneralVariables
import com.example.myapplication.R
import com.example.myapplication.model.MovieData

class MoviesAdapter(private val contexto: Context) : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    lateinit var dataSet: List<MovieData>



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titulo: TextView
        val calificacion: TextView
        val imagen: ImageView
        val contenedor: LinearLayout

        init {
            titulo = view.findViewById(R.id.titulo)
            calificacion = view.findViewById(R.id.calificacion)
            imagen = view.findViewById(R.id.imagen)
            contenedor = view.findViewById(R.id.contenedor)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_movie, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {


        Glide.with(contexto)
            .load(GeneralVariables.baseUrlImage + dataSet[position].backdrop_path)
            .into(viewHolder.imagen);

        viewHolder.titulo.text = dataSet[position].title
        viewHolder.calificacion.text = dataSet[position].vote_average.toString()

        viewHolder.contenedor.setOnClickListener {


            contexto.startActivity(
                Intent(contexto, DetalleActivity::class.java).apply {

                    putExtra("idPelicula", dataSet[position].id)

            })


        }

    }

    override fun getItemCount() = dataSet.size

}