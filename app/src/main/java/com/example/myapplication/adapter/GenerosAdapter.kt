package com.example.myapplication.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.model.Genero

class GenerosAdapter(private val contexto: Context) : RecyclerView.Adapter<GenerosAdapter.ViewHolder>() {

    lateinit var dataSet: List<Genero>



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nombre: TextView


        init {
            nombre = view.findViewById(R.id.nombre_genero)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_genero, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {


        viewHolder.nombre.text = dataSet[position].name

    }

    override fun getItemCount() = dataSet.size

}