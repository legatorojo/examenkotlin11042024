package com.example.myapplication.repository

import android.util.Log
import com.example.examenkotlin27032024.Remote.GetDataApi
import com.example.myapplication.remote.FlowEnum
import com.example.myapplication.remote.url
import kotlinx.coroutines.flow.flow

class MoviesRepository {


    private val remote =
        RetrofitInstance.getRetrofitInstance().create(GetDataApi::class.java)


    suspend fun GetListMovies(
    ) = flow {
        try {
            val response = remote.getListMovies(
                url = FlowEnum.GET_LISTADO.url()
            )

            emit(response.body())
        } catch (e: Exception) {
            Log.e("ERROR", e.stackTraceToString())
        }
    }


    suspend fun GetMovieData(
        id: Long
    ) = flow {
        try {
            val response = remote.getMovieData(
                url = FlowEnum.GET_DATA.url( arrayOf(id.toString()) )
            )

            emit(response.body())
        } catch (e: Exception) {
            Log.e("ERROR", e.stackTraceToString())
        }
    }

}